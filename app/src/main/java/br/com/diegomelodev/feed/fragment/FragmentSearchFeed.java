package br.com.diegomelodev.feed.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.diegomelodev.feed.R;
import br.com.diegomelodev.feed.activity.MainActivity;

/**
 * Created by diegom on 07/04/2017.
 */

public class FragmentSearchFeed extends Fragment {
    private static final String TAG = FragmentSearchFeed.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_feed, container, false);
        final EditText edtUrl = (EditText) view.findViewById(R.id.edtSearchFeed);
        edtUrl.setText("https://jovemnerd.com.br/feed-nerdcast/");

        Button brnSearch = (Button) view.findViewById(R.id.btnSearch);
        brnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = edtUrl.getText().toString();
                String message = getResources().getString(R.string.msg_required_url_feed);
                if (!url.trim().isEmpty()) {
                    MainActivity activity = (MainActivity) getActivity();
                    activity.addItemMenu(url);
                    message = getResources().getString(R.string.msg_success_subscribe);
                    activity.replaceFragment(new FragmentFeed(), url);
                }
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }
}
