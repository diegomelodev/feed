package br.com.diegomelodev.feed.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Map;

import javax.inject.Inject;

import br.com.diegomelodev.feed.App;
import br.com.diegomelodev.feed.R;
import br.com.diegomelodev.feed.fragment.FragmentFeed;
import br.com.diegomelodev.feed.fragment.FragmentSearchFeed;
import br.com.diegomelodev.feed.util.StringUtil;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private NavigationView navigationView;
    private DrawerLayout drawer;

    @Inject
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App) getApplication()).getPreferencesComponent().inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        initMenu();
        replaceFragment(new FragmentSearchFeed());
    }

    /**
     * Monta os itens do menu com base nos dados salvos no SharedPreferences
     */
    private void initMenu() {
        Map<String, String> all = (Map<String, String>) preferences.getAll();
        for (Map.Entry<String, String> entry : all.entrySet()) {
            String url = entry.getValue();
            saveInPreferences(url, StringUtil.formatTitle(url));
        }
    }

    public void addItemMenu(String url) {
        //TODO obter o nome do feed no próprio RSS
        String title = StringUtil.formatTitle(url);
        if(!hasSubscription(title)) {
            saveInPreferences(url, title);
        }
    }

    private void saveInPreferences(String url, String title) {
        preferences.edit()
                .putString(title, url)
                .apply();
        navigationView.getMenu().add(R.id.menuGroupSubscriptions, Menu.NONE, Menu.NONE, title);
    }

    private boolean hasSubscription(String title) {
        return preferences.getString(title, null) == null;
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void replaceFragment(Fragment fragment, String url) {
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        fragment.setArguments(bundle);
        replaceFragment(fragment);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentFragment, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        String title = (String) item.getTitle();
        if ("Buscar Feeds".equals(title)) {
            replaceFragment(new FragmentSearchFeed());
        } else {
            Map<String, String> all = (Map<String, String>) preferences.getAll();
            for (Map.Entry<String, String> entry : all.entrySet()) {
                if(entry.getKey().equals((title))) {
                    replaceFragment(new FragmentFeed(), entry.getValue());
                    break;
                }
            }
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
