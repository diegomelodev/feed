package br.com.diegomelodev.feed.util;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by diegom on 07/04/2017.
 */

public class DateUtil {
    private static final String TAG = DateUtil.class.getSimpleName();

    private DateUtil() {}

    public static Calendar formatDate(String value) {
        Calendar calendar = null;
        try {
            DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz",  Locale.US);
            Date date = formatter.parse(value);
            calendar = Calendar.getInstance();
            calendar.setTime(date);
        } catch (ParseException e) {
            Log.e(TAG, "Falha ao converter data para o valor " + value, e);
        }
        return calendar;
    }
}
