package br.com.diegomelodev.feed.model.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by diegom on 06/04/2017.
 */

@Root(name = "rss", strict = false)
public class Rss {

    @Element(name = "channel")
    private Channel channel;

    public Channel getChannel() {
        if(channel == null) {
            channel = new Channel();
        }
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}

