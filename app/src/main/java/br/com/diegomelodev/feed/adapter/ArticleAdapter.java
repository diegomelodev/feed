package br.com.diegomelodev.feed.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;
import java.util.Calendar;
import java.util.List;

import br.com.diegomelodev.feed.R;
import br.com.diegomelodev.feed.activity.WebViewActivity;
import br.com.diegomelodev.feed.model.dto.Article;
import br.com.diegomelodev.feed.util.DateUtil;

/**
 * Created by diegom on 06/04/2017.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> {
    private static final String TAG = ArticleAdapter.class.getSimpleName();

    private List<Article> articles;
    private Context context;

    public ArticleAdapter(Context context, List<Article> articles) {
        this.context = context;
        this.articles = articles;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txvTitle;
        public TextView txvDate;
        public ImageView imgPreview;

        public ViewHolder(View view) {
            super(view);
            txvTitle = (TextView) view.findViewById(R.id.txvTitle);
            txvDate = (TextView) view.findViewById(R.id.txvDate);
            imgPreview = (ImageView) view.findViewById(R.id.imgPreview);
        }
    }

    @Override
    public ArticleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.model_article, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Article article = articles.get(position);
        holder.txvTitle.setText(article.getTitle());
        String imageSrc = getUrlImage(article);
        if(imageSrc == null) {
            holder.imgPreview.setVisibility(View.GONE);
        } else {
            Picasso.with(context).
                    load(imageSrc)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .into(holder.imgPreview);
        }

        Calendar pubDate = DateUtil.formatDate(article.getPubDate());
        holder.txvDate.setText(formatTextTime(pubDate));

        holder.itemView.setOnClickListener(onItemClick(article));
    }

    @NonNull
    private View.OnClickListener onItemClick(final Article article) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("url", article.getLink());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeCustomAnimation(context, R.anim.enter, R.anim.exit);
                    context.startActivity(intent, options.toBundle());
                } else {
                    context.startActivity(intent);
                }
            }
        };
    }

    @NonNull
    private String formatTextTime(Calendar pubDate) {
        String textTime = "";
        if(pubDate == null) {
            return textTime;
        }
        Calendar now = Calendar.getInstance();
        long millis = now.getTimeInMillis() - pubDate.getTimeInMillis();
        long hours = millis / 1000 / 60 / 60;
        if(hours > 24) {
            textTime = "Ontem";
        } else if(hours > 48 ) {
            textTime = "2 dias atrás";
        } else if(hours > 72) {
            textTime = "Alguns dias atrás atrás";
        } else if(hours == 0) {
            textTime = "Última hora";
        } else {
            textTime = hours + " horas atrás";
        }
        return textTime;
    }

    @Nullable
    private String getUrlImage(Article article) {
        String imageSrc = null;
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new ByteArrayInputStream(article.getDescription().getBytes()), null);
            while (imageSrc == null && parser.next() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                if("img".equals(parser.getName())) {
                    for(int i = 0; i < parser.getAttributeCount(); i++) {
                        if("src".equals(parser.getAttributeName(i))) {
                            imageSrc = parser.getAttributeValue(i);
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Falha ao obter imagem do artigo", e);
        }
        return imageSrc;
    }



    @Override
    public int getItemCount() {
        return articles != null ? articles.size() : 0;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
        this.notifyDataSetChanged();
    }
}