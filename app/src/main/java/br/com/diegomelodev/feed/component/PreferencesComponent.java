package br.com.diegomelodev.feed.component;

import android.content.SharedPreferences;

import javax.inject.Singleton;

import br.com.diegomelodev.feed.activity.MainActivity;
import br.com.diegomelodev.feed.module.PreferencesModule;
import dagger.Component;

/**
 * Created by diegom on 06/04/2017.
 */


@Singleton
@Component(modules = {PreferencesModule.class})
public interface PreferencesComponent {

    SharedPreferences providePreferences();

    void inject(MainActivity activity);


}