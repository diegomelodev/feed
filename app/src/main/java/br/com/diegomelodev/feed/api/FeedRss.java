package br.com.diegomelodev.feed.api;


import br.com.diegomelodev.feed.model.dto.Rss;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by diegom on 06/04/2017.
 */

public interface FeedRss {

    @GET
    Observable<Rss> getFeedRss(@Url String url);


}
