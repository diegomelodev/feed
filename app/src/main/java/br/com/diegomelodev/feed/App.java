package br.com.diegomelodev.feed;

import android.app.Application;

import br.com.diegomelodev.feed.component.DaggerNetworkComponent;
import br.com.diegomelodev.feed.component.DaggerPreferencesComponent;
import br.com.diegomelodev.feed.component.NetworkComponent;
import br.com.diegomelodev.feed.component.PreferencesComponent;
import br.com.diegomelodev.feed.module.AppModule;
import br.com.diegomelodev.feed.module.NetworkModule;
import br.com.diegomelodev.feed.module.PreferencesModule;

/**
 * Created by diegom on 06/04/2017.
 */

public class App extends Application {

    private NetworkComponent networkComponent;
    private PreferencesComponent preferencesComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        networkComponent = DaggerNetworkComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();

        preferencesComponent = DaggerPreferencesComponent.builder()
                .preferencesModule(new PreferencesModule(this))
                .build();
    }

    public NetworkComponent getNetworkComponent() {
        return networkComponent;
    }

    public PreferencesComponent getPreferencesComponent() {
        return preferencesComponent;
    }
}
