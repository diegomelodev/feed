package br.com.diegomelodev.feed.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.diegomelodev.feed.App;
import br.com.diegomelodev.feed.R;
import br.com.diegomelodev.feed.adapter.ArticleAdapter;
import br.com.diegomelodev.feed.api.FeedRss;
import br.com.diegomelodev.feed.model.dto.Rss;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by diegom on 07/04/2017.
 */

public class FragmentFeed extends Fragment {
    private static final String TAG = FragmentFeed.class.getSimpleName();

    @Inject
    Retrofit retrofit;

    private ArticleAdapter adapter;
    private SwipeRefreshLayout swipeRefresh;
    private String url;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getNetworkComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        url = getArguments().getString("url");

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.reciclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new ArticleAdapter(getContext(), null);
        recyclerView.setAdapter(adapter);

        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callRss(url);

            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        swipeRefresh.setRefreshing(true);
        callRss(url);
    }

    private void callRss(String url) {
        Observable<Rss> feedRss = retrofit.create(FeedRss.class).getFeedRss(url);
        feedRss.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Rss>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(TAG, "onSubscribe");
                    }

                    @Override
                    public void onNext(Rss rss) {
                        adapter.setArticles(rss.getChannel().getArticleList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getContext(), "Não foi possível carregar esse Feed", Toast.LENGTH_LONG).show();
                        Log.e(TAG, "Falha ao carregar feed", e);
                        swipeRefresh.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onComplete");
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}
