package br.com.diegomelodev.feed.component;

import javax.inject.Singleton;

import br.com.diegomelodev.feed.fragment.FragmentFeed;
import br.com.diegomelodev.feed.module.AppModule;
import br.com.diegomelodev.feed.module.NetworkModule;
import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by diegom on 06/04/2017.
 */


@Singleton
@Component(modules = {NetworkModule.class, AppModule.class})
public interface NetworkComponent {

    Retrofit provideRetrofit();

    void inject(FragmentFeed fragment);


}