package br.com.diegomelodev.feed.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by diegom on 07/04/2017.
 */

@Module
public class PreferencesModule {
    private Application app;
    private String PREF_NAME = "prefs";

    public PreferencesModule(Application app) {
        this.app = app;
    }

    @Singleton
    @Provides
    public SharedPreferences getAppPreferences() {
        return app.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }
}
