package br.com.diegomelodev.feed.util;

/**
 * Created by diegom on 07/04/2017.
 */

public class StringUtil {

    private StringUtil() {}

    /**
     * Formata o titulo dos feeds com base na sua URL
     * @param url
     * @return
     */
    public static String formatTitle(String url) {
        if(url == null) {
            return url;
        }
        url = url.toLowerCase()
                .replace("http://", "")
                .replace("https://", "")
                .replace("www.", "");
        if(url.contains("/")) {
            return url.substring(0, url.indexOf("/"));
        } else {
            return url.substring(0, url.indexOf("."));
        }
    }
}
