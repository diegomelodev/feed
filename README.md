# README #

Feed é um App Android para leitura de RSS.

### Funcionalidades ###
* Possibilidade de adicionar um novo RSS através de um url;
* Listagem de feeds adicionados em um Drawer menu;
* Listagem dos itens de um feed adicionado mostrando data de publicação, título, descrição e miniatura;
* Possibilidade de visualizar um item do feed;
* Cache(em memória) de itens antigos;
* Sincronização de conteúdo de um feed com pull refresh;

### Bibliotecas Utilizadas ###
* Dagger2 - Injeção de dependência
* Picasso - Carregamento imagens
* Retrofit - Cliente HTTP
* RxAndroid - Programação Reativa